﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    public class LoggerUtil
    {

        public static void TraceInfo(string methodName, string message)
        {
            Trace.TraceInformation(string.Format(";{0};{1};{2}", DateTime.Now.ToString("s"), methodName, message));
        }

        public static void TraceInfoDuration(string methodName, DateTime start)
        {
            Trace.TraceInformation(string.Format(";{0};{1};{2};{3}", DateTime.Now.ToString("s"), methodName, "Duration", (DateTime.Now - start).TotalMilliseconds));
        }

        public static void TraceError(string methodName, string exceptionMessage)
        {
            Trace.TraceError(string.Format(";{0};{1};{2};{3}", DateTime.Now.ToString("s"), methodName, "Exception", exceptionMessage));
        }

        // støtter Altinn .Net 4.5 ??
        public static void TraceInfoV45(string message, [CallerMemberName] string methodName = "")
        {
            Trace.TraceInformation(string.Format(";{0};{1};{2}", DateTime.Now.ToString("s"), methodName, message));
        }


    }
}