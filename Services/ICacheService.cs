﻿using System;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    public interface ICacheService
    {
        /// <summary>
        /// Get cached value
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string Get(string key);

        /// <summary>
        /// Add item to cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="item"></param>
        /// <param name="expirationDateTime"></param>
        void Add(string key, string item, DateTimeOffset expirationDateTime);
    }
}
