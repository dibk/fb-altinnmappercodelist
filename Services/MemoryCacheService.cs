﻿using System;
using System.Runtime.Caching;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    public class MemoryCacheService : ICacheService
    {
        public string Get(string key)
        {   
            return (string) MemoryCache.Default.Get(key);
        }

        public void Add(string key, string item, DateTimeOffset expirationDateTime)
        {
            MemoryCache.Default.Add(key, item, expirationDateTime);
        }
    }
}