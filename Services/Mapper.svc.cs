﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Web.Configuration;
using Newtonsoft.Json;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class Mapper : IMapper
    {
        private readonly ICodeListService _codeListService;

        public Mapper()
        {
            _codeListService = new CodeListService();
        }

        public Mapper(ICodeListService codeListService)
        {
            _codeListService = codeListService;
        }

        /// <summary>
        /// Checks the validity of a code list value.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public IsValidCodeListRespons IsValidCodeListValue(IsValidCodeListForespoersel request)
        {
            DateTime start = DateTime.Now;
            string methodName = MethodBase.GetCurrentMethod().Name;

            var response = new IsValidCodeListRespons();
            try
            {
                Dictionary<string, string> dictionary = GetCodeListDictionary(request);
                response.IsValidCodeListValue = dictionary.ContainsKey(request.CodeListValue);
                LoggerUtil.TraceInfo(methodName, "IsValidCodeListValue: " + response.IsValidCodeListValue + " [codeListValue=" + request.CodeListValue + ", codeListUrl=" + request.CodeListUrl + ", codeListId=" + request.CodeListId + "]");
            }
            catch (Exception e)
            {
                response.Status = "Exception while checking code list value. " + e.Message;
                LoggerUtil.TraceError(methodName, e.Message);
            }
            LoggerUtil.TraceInfoDuration(methodName, start);
            return response;
        }

        /// <summary>
        /// Returns all valid code list values for a given codelist.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetCodeListValuesRespons GetCodeListValues(GetCodeListValuesForespoersel request)
        {
            DateTime start = DateTime.Now;
            string methodName = MethodBase.GetCurrentMethod().Name;

            var response = new GetCodeListValuesRespons();
            try
            {
                response.CodeListValues =
                    GetCodeListDictionary(request).Select(c => new CodeListValue() {Description = c.Value, Value = c.Key}).ToList();
                LoggerUtil.TraceInfo(methodName, "Returning codelist [codelistUrl=" + request.CodeListUrl + ", codelistId=" + request.CodeListId +"] with " + response.CodeListValues.Count + " values.");
            }
            catch (Exception e)
            {
                string statusMessage = "Exception while getting code list values for [codelistUrl=" + request.CodeListUrl + ", codelistId=" + request.CodeListId + "]. " + e.Message;
                response.Status = statusMessage;
                LoggerUtil.TraceError(methodName, statusMessage);
            }

            LoggerUtil.TraceInfoDuration(methodName, start);

            return response;
        }
        private Dictionary<string, string> GetCodeListDictionary(IsValidCodeListForespoersel request)
        {
            return GetCodeListDictionary(request.CodeListUrl, request.CodeListId);
        }

        private Dictionary<string, string> GetCodeListDictionary(GetCodeListValuesForespoersel request)
        {
            return GetCodeListDictionary(request.CodeListUrl, request.CodeListId);
        }

        private Dictionary<string, string> GetCodeListDictionary(string codeListUrl, string codeListId)
        {
            Dictionary<string, string> dictionary;
            if (!string.IsNullOrWhiteSpace(codeListUrl))
            {
                dictionary = _codeListService.GetCodelistByUrl(codeListUrl);
            }
            else
            {
                dictionary = _codeListService.GetCodelistById(codeListId);
            }
            return dictionary;
        }



        /// <summary>
        /// Returns validations results for a submitted XML.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ValidationResultRespons Validate(ValidateFormForespoersel request)
        {
            DateTime start = DateTime.Now;
            string methodName = MethodBase.GetCurrentMethod().Name;

            var response = new ValidationResultRespons();
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

                    client.BaseAddress = new Uri(WebConfigurationManager.AppSettings["ftbValidationApiUrl"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage httpresponse = client.PostAsync("api/validate/form", content).Result;
                    if (httpresponse.IsSuccessStatusCode)
                    {
                        string data = httpresponse.Content.ReadAsStringAsync().Result;
                        response = JsonConvert.DeserializeObject<ValidationResultRespons>(data);
                    }
                    else
                    {
                        LoggerUtil.TraceError(methodName, httpresponse.StatusCode.ToString() + " " + httpresponse.Content.ToString());
                    }

                    LoggerUtil.TraceInfo(methodName, "Validate: " + request.DataFormatId + " - " + request.DataFormatVersion + " result(error,warning): " + response.errors + ", " + response.warnings);
                }
            }
            catch (Exception e)
            {
                //response.Status = "Exception while checking code list value. " + e.Message;
                LoggerUtil.TraceError(methodName, e.Message);
            }
            LoggerUtil.TraceInfoDuration(methodName, start);
            return response;
        }
    }
}
