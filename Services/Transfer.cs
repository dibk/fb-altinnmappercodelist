﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    [DataContract]
    public class IsValidCodeListForespoersel
    {
        /// <summary>
        /// GUID of a code list registered in Geonorge Registry
        /// </summary>
        [DataMember] public string CodeListId;

        /// <summary>
        /// Partial url to a code list (relative url from register.geonorge.no/api/) registered in Geonorge Registry. E.g. subregister/sosi-kodelister/kartverket/fylkesnummer
        /// </summary>
        [DataMember] public string CodeListUrl;

        /// <summary>
        /// The code list value to check validity of.
        /// </summary>
        [DataMember] public string CodeListValue;
    }

    [DataContract]
    public class IsValidCodeListRespons
    {
        [DataMember] public bool IsValidCodeListValue;
        [DataMember] public string Status = "OK";
    }

    [DataContract]
    public class GetCodeListValuesForespoersel
    {
        /// <summary>
        /// GUID of a code list registered in Geonorge Registry
        /// </summary>
        [DataMember] public string CodeListId;
        /// <summary>
        /// Partial url to a code list (relative url from register.geonorge.no/api/) registered in Geonorge Registry. E.g. subregister/sosi-kodelister/kartverket/fylkesnummer
        /// </summary>
        [DataMember]
        public string CodeListUrl;
    }

    [DataContract]
    public class GetCodeListValuesRespons
    {
        [DataMember] public List<CodeListValue> CodeListValues = new List<CodeListValue>();
        [DataMember] public string Status = "OK";
    }

    public class CodeListValue
    {
        [DataMember] public string Value;
        [DataMember] public string Description;

    }

    [DataContract]
    public class ValidateFormForespoersel
    {
        /// <summary>
        /// DataFormatId of the Form. https://www.altinn.no/api/help
        /// </summary>
        [DataMember]
        public string DataFormatId { get; set; }
        /// <summary>
        /// DataFormatVersion of the Form. https://www.altinn.no/api/help
        /// </summary>
        [DataMember]
        public string DataFormatVersion { get; set; }
        /// <summary>
        /// The Form data. https://www.altinn.no/api/help
        /// </summary>
        [DataMember]
        public string FormData { get; set; }

        /// <summary>
        /// List of all attachment types and forms/subforms name used to validate required documentation
        /// </summary>
        [DataMember]
        public string[] AttachmentTypesAndForms { get; set; }
    }


    [DataContract]
    public class ValidationResultRespons
    {
        [DataMember]
        public int warnings;
        [DataMember]
        public int errors;
        [DataMember]
        public List<ValidationMessage> messages;

    }
    [DataContract]
    public class ValidationMessage
    {
        [DataMember]
        public string message;
        [DataMember]
        public string messagetype;
        [DataMember]
        public string reference;
        [DataMember]
        public string xpathField;
    }



}