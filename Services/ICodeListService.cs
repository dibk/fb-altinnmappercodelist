﻿using System.Collections.Generic;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    public interface ICodeListService
    {
        Dictionary<string, string> GetCodelistById(string codeListId);

        Dictionary<string, string> GetCodelistByUrl(string codeListUrl);
    }
}
