﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Configuration;
using Newtonsoft.Json.Linq;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    public class CodeListService : ICodeListService
    {
        private readonly ICacheService _cacheService;
        private readonly ICodeListDownloader _codeListDownloader;

        public CodeListService()
        {
            _cacheService = new MemoryCacheService();
            _codeListDownloader = new CodeListDownloader();
        }

        public CodeListService(ICacheService cacheService, ICodeListDownloader codeListDownloader)
        {
            _cacheService = cacheService;
            _codeListDownloader = codeListDownloader;
        }

        public Dictionary<string, string> GetCodelistById(string codeListId)
        {
            string rawCodeList = DownloadCodeList(ResolveCompleteUrlForCodeListId(codeListId));
            return ParseCodeList(rawCodeList);
        }

        public Dictionary<string, string> GetCodelistByUrl(string codeListUrl)
        {
            string rawCodeList = DownloadCodeList(ResolveCompleteUrlForCodeListUrl(codeListUrl));
            return ParseCodeList(rawCodeList);
        }

        private Dictionary<string, string> ParseCodeList(string data)
        {
            var codeList = new Dictionary<string, string>();

            var response = JObject.Parse(data);

            foreach (var code in response["containeditems"])
            {
                var codevalue = code["codevalue"].ToString();

                if (string.IsNullOrWhiteSpace(codevalue))
                    codevalue = code["label"].ToString();

                if (!codeList.ContainsKey(codevalue))
                {
                    codeList.Add(codevalue, code["label"].ToString());
                }
            }

            return codeList;
        }

        private string DownloadCodeList(string codeListUrl)
        {
            var cachedValue = _cacheService.Get(codeListUrl);

            if (cachedValue != null)
            {
                LoggerUtil.TraceInfo(MethodBase.GetCurrentMethod().Name, "Found code list [" + codeListUrl + "] in cache.");
                return cachedValue;
            }

            LoggerUtil.TraceInfo(MethodBase.GetCurrentMethod().Name, "Code list [" + codeListUrl + "] not cached.");
            
            var codeList = _codeListDownloader.DownloadCodeList(codeListUrl);

            LoggerUtil.TraceInfo(MethodBase.GetCurrentMethod().Name, "Adding code list [" + codeListUrl + "] to cache.");
            _cacheService.Add(codeListUrl, codeList, DateTime.Now.AddMinutes(60));

            return codeList;
        }

        private string ResolveCompleteUrlForCodeListId(string codeListId)
        {
            return WebConfigurationManager.AppSettings["GeonorgeRegistryApiUrl"] + "kodelister/" + codeListId;
        }

        private string ResolveCompleteUrlForCodeListUrl(string codeListUrl)
        {
            return WebConfigurationManager.AppSettings["GeonorgeRegistryApiUrl"] + codeListUrl;
        }
    }
}