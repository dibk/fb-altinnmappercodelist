﻿using System.ServiceModel;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    [ServiceContract]
    public interface IMapper
    {
        [OperationContract]
        IsValidCodeListRespons IsValidCodeListValue(IsValidCodeListForespoersel request);

        [OperationContract]
        GetCodeListValuesRespons GetCodeListValues(GetCodeListValuesForespoersel request);

        [OperationContract]
        ValidationResultRespons Validate(ValidateFormForespoersel request);

    }


}
