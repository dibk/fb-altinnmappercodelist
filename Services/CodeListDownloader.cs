﻿using System;
using System.Reflection;
using System.Text;

namespace DIBK.FBygg.Altinn.MapperCodelist
{
    public class CodeListDownloader : ICodeListDownloader
    {
        public string DownloadCodeList(string url)
        {
            DateTime start = DateTime.Now;
            LoggerUtil.TraceInfo(MethodBase.GetCurrentMethod().Name, "Downloading codelist from " + url);

            string result;
            using (System.Net.WebClient client = new System.Net.WebClient())
            {
                client.Encoding = Encoding.UTF8;
                result = client.DownloadString(url);
            }

            LoggerUtil.TraceInfoDuration(MethodBase.GetCurrentMethod().Name, start);

            return result;
        }

    }
}