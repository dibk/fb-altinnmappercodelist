using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DIBK.FBygg.Altinn.MapperCodelist;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Tests
{
    [TestClass]
    public class CodeListServiceTest
    {
        [TestMethod]
        public void ShouldReturnCodeListFromGeonorgeWhenNotCached()
        {
            string codeListId = "12345";

            var cacheMock = new Mock<ICacheService>();
            cacheMock.Setup(c => c.Get(It.IsAny<string>())).Returns<string>(null);

            var downloaderMock = new Mock<ICodeListDownloader>();
            downloaderMock.Setup(d => d.DownloadCodeList(It.IsAny<string>())).Returns(GetExampleData());
            var codeListService = new CodeListService(cacheMock.Object, downloaderMock.Object);

            Dictionary<string, string> codeList = codeListService.GetCodelistById(codeListId);
            Assert.IsNotNull(codeList);
            Assert.AreEqual(2, codeList.Keys.Count);
        }

        [TestMethod]
        public void ShouldReturnCachedCodeList()
        {
            string codeListId = "12345";

            var cacheMock = new Mock<ICacheService>();
            cacheMock.Setup(c => c.Get(It.IsAny<string>())).Returns(GetExampleData());

            var downloaderMock = new Mock<ICodeListDownloader>();
            var codeListService = new CodeListService(cacheMock.Object, downloaderMock.Object);
            downloaderMock.Setup(d => d.DownloadCodeList(It.IsAny<string>())).Returns(GetExampleData());

            Dictionary<string, string> codeList = codeListService.GetCodelistById(codeListId);

            downloaderMock.Verify(m => m.DownloadCodeList(It.IsAny<string>()), Times.Never);

            Assert.IsNotNull(codeList);
            Assert.AreEqual(2, codeList.Keys.Count);
        }

        private static string GetExampleData()
        {
            string exampleData = File.ReadAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "codelist.json"));
            return exampleData;
        }
        
    }
}
