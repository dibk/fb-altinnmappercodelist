﻿using System.Collections.Generic;
using DIBK.FBygg.Altinn.MapperCodelist;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Tests
{
    [TestClass]
    public class MapperTest
    {
        [TestMethod]
        public void ShouldReturnTrueWhenCodeListValueIsValid()
        {
            var codeListId = "12345";
            var codeListValue = "key";

            Dictionary<string, string> codeList = new Dictionary<string, string> { {codeListValue, "Description of key"} };
            var mapper = CreateMapperWithMock(codeListId, codeList);

            IsValidCodeListRespons response = mapper.IsValidCodeListValue(new IsValidCodeListForespoersel() {CodeListId = codeListId, CodeListValue = codeListValue});

            Assert.IsTrue(response.IsValidCodeListValue);
        }

        [TestMethod]
        public void ShouldReturnFalseWhenCodeListValueIsInvalid()
        {
            var codeListId = "12345";

            Dictionary<string, string> codeList = new Dictionary<string, string> { { "key", "Description of key" } };
            var mapper = CreateMapperWithMock(codeListId, codeList);

            IsValidCodeListRespons response = mapper.IsValidCodeListValue(new IsValidCodeListForespoersel() { CodeListId = codeListId, CodeListValue = "key-that-does-not-exist" });

            Assert.IsFalse(response.IsValidCodeListValue);
        }

        [TestMethod]
        public void ShouldReturnCodeListValues()
        {
            var codeListId = "12345";

            Dictionary<string, string> codeList = new Dictionary<string, string> { { "key", "Description of key" }, {"another-key", "yet another description"} };
            var mapper = CreateMapperWithMock(codeListId, codeList);

            GetCodeListValuesRespons response = mapper.GetCodeListValues(new GetCodeListValuesForespoersel {CodeListId = codeListId});

            Assert.AreEqual(2, response.CodeListValues.Count);
        }


        private static Mapper CreateMapperWithMock(string codeListId, Dictionary<string, string> codeList)
        {
            var mock = new Mock<ICodeListService>();
            mock.Setup(c => c.GetCodelistById(codeListId)).Returns(codeList);

            var mapper = new Mapper(mock.Object);
            return mapper;
        }
    }
}
